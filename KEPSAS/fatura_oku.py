#!/usr/bin/env python3

from datetime import datetime
from pathlib import Path
import pdfplumber
import traceback
import openpyxl
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade setuptools
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
# sudo python3 -m pip install --upgrade typing_extensions
#################################################

bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']


def fatura_oku():
	try:
		# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
		# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
		pdf_klasoru = Path('./PDF/').rglob('*.pdf')
		    # PDF Klasörü içindeki her bir dosyayı buluyoruz.
		files = [x for x in pdf_klasoru]
		    # PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
		sira_no = 0

		# Uçbirimde çıkacak sutun isimleri
		print("{:<5}{:<10}{:<55}{:<10}{:<20}".format("S.N", "TEKİL NO", "CAMİ/ABONE ADI", "TUTAR", "DOSYA ADI"))

		birincisayfa = []
		# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
		for file in files:
			# pdfplumber modülü ile dosyaları açıyoruz
			with pdfplumber.open(file) as pdf:
				ilksayfa = pdf.pages[0]
				birincisayfa.append(ilksayfa.extract_text())
			#print(birincisayfa)
			#print(birincisayfa[sira_no])

			sira_no = sira_no + 1
			# Sayfaları Geziyoruz
			for sayfa in birincisayfa:
				# Cami/Abone adını tespit etmek için kriter belirliyoruz.
				unvan_re = "\/Unvan\s:(\D+)\selek|\/Unvan\W+(.+)\\n"
				unvan_eslesen = re.search(unvan_re, sayfa)

				unvan_temizle = str(unvan_eslesen.group()).split(":")[1].split("Adres")[0].split("\n")[0].split("Kapı No:")[0].split("Kapı")[0].split("No:")[0].replace("MH.", "MAH.")
				if "CAMİ" in unvan_temizle or "CAMI" in unvan_temizle:
					abone_adi = f"{unvan_temizle.split('CAMİ')[0].split('CAMI')[0].strip()} CAMİİ"
				else:
					abone_adi = unvan_temizle

				# Toplam harcanan KWH
				kwh_eslesen = re.search(r"\(Endeks\)\s?(\S+)\s(\S+)\s(\S+)\s", sayfa)
				kwh_toplam = float(' '.join(kwh_eslesen.groups()).split()[-1])

				# Fatura Tesisat/Tüketici Numarası
				tekilkod_eslesen = re.search("Kodu\\n(\S+)\s(\S+)\s(\S+)\\n", sayfa)
				tekilkod = int(' '.join(tekilkod_eslesen.groups()).split()[0])

				# Fatura Tesisat/Tüketici Numarası
				etso_eslesen = re.search("Kodu\\n(\S+)\s(\S+)\s(\S+)\\n", sayfa)
				etso_no = ' '.join(etso_eslesen.groups()).split()[-1]

				# Fatura No
				fatura_eslesen = re.search("No:\s(KE\d+)\s", sayfa)
				fatura_no = ' '.join(fatura_eslesen.groups()).strip()

				# Müşteri Grubu
				grubu_eslesen = re.search("Grubu\s?:?(\D+)", sayfa)
				grubu = ' '.join(grubu_eslesen.groups(1)).split("Elekt")[0].split("son")[0].split("Tüketici")[0].strip()

				# Faturanın İlk ve Son Okuma Tarihleri
				ilksonOkuma_eslesen = re.search("Okuma\(\D+\)\s(\d+.\d+.\d+)\s(\d+.\d+.\d+)", sayfa)
				ilkOkuma_tarihi = ' '.join(ilksonOkuma_eslesen.groups()).split()[0].replace("/", ".")
				sonOkuma_tarihi = ' '.join(ilksonOkuma_eslesen.groups()).split()[1].replace("/", ".")

				# Faturanın ait olduğu dönem
				faturaDonemi_yil = sonOkuma_tarihi.split(".")[-1]
				faturaDonemi_ay = sonOkuma_tarihi.split(".")[1]
				faturaDonemi = faturaDonemi_ay + "/" + faturaDonemi_yil

				# Son Ödeme Tarihi
				sonOdemetarihi_eslesen = re.search("\s(\d{2}\/\d{2}\/\d{4})\s?(o|u|T|E|\\n)", sayfa)
				sonOdeme_tarihi = ' '.join(sonOdemetarihi_eslesen.groups(1)).split()[0]

				# KDV Dahil Fatura Tutarı
				faturaTutari_eslesen = re.search("[F]\w{5}.T\D+([0-9]+.?[0-9]+.?\d+)", sayfa)
				faturaTutari = float(' '.join(faturaTutari_eslesen.groups()).replace('.', '').replace(',', '.').strip())

				# fatura tutarına binde 948 ile çarpılarak bulunmuştur.
				damga_vergisi = round(((faturaTutari/1.18)*0.00948), 2)

				# Fatura tutarını 1.18 e bölüp yüzde 18 ile çarparak bulunmuştur.
				kdv = round(((faturaTutari/1.18)*0.18), 2)

				# Önceki döneme ait borç
				oncekiBorc_eslesen = re.search("B\D+\/B\D+([0-9]+.?[0-9]+.?\d+)", sayfa)
				if oncekiBorc_eslesen == None:
					# Eğer borç yoksa sıfır yaz
					oncekiBorcTutari = float(0)
				else:
					oncekiBorcTutari = float(' '.join(oncekiBorc_eslesen.groups(1)).strip().replace('.', '').replace(',', '.').strip())

				# Ödenecek Tutar
				odenecekTutar_eslesen = re.search(".denecek\s[T]\w{4}\s([0-9]+.?[0-9]+.?\d+)", sayfa)
				odenecekTutar = float(' '.join(odenecekTutar_eslesen.groups()).replace('.', '').replace(',', '.').strip())

			# Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
			print("{:<5}{:<10}{:<55}{:<10}{:<20}".format(sira_no, tekilkod, abone_adi, odenecekTutar, str(file)))

			# ornek_excel.xlsx dosyasının son satırını buluyoruz.
			son_satir = excel_sayfasi.max_row

			# ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
			excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
			excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(tekilkod)
			excel_sayfasi.cell(column=3, row=son_satir + 1).value = etso_no
			excel_sayfasi.cell(column=4, row=son_satir + 1).value = fatura_no
			excel_sayfasi.cell(column=5, row=son_satir + 1).value = abone_adi
			excel_sayfasi.cell(column=6, row=son_satir + 1).value = grubu
			excel_sayfasi.cell(column=7, row=son_satir + 1).value = faturaDonemi
			excel_sayfasi.cell(column=8, row=son_satir + 1).value = ilkOkuma_tarihi
			excel_sayfasi.cell(column=9, row=son_satir + 1).value = sonOkuma_tarihi
			excel_sayfasi.cell(column=10, row=son_satir + 1).value = sonOdeme_tarihi
			excel_sayfasi.cell(column=11, row=son_satir + 1).value = kwh_toplam
			excel_sayfasi.cell(column=12, row=son_satir + 1).value = damga_vergisi
			excel_sayfasi.cell(column=13, row=son_satir + 1).value = kdv
			excel_sayfasi.cell(column=14, row=son_satir + 1).value = faturaTutari
			excel_sayfasi.cell(column=15, row=son_satir + 1).value = oncekiBorcTutari
			excel_sayfasi.cell(column=16, row=son_satir + 1).value = odenecekTutar

			# ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
			# pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
			excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')

	except:
		print("Dosya Adı: ", file)
		print(traceback.format_exc())

if __name__ == "__main__":
	fatura_oku()
