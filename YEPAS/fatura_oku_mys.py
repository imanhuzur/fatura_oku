#!/usr/bin/env python3

from datetime import datetime
from pathlib import Path
import pdfplumber
import traceback
import openpyxl
import math
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
#################################################

#Tarih
bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

def fatura_oku():
	try:
		# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
		# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
		pdf_klasoru = Path('./PDF/').rglob('*.pdf')
		    # PDF Klasörü içindeki her bir dosyayı buluyoruz.
		files = [x for x in pdf_klasoru]
		    # PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
		sira_no = 0

		# Uçbirimde çıkacak sutun isimleri
		#print("{:<5}{:<18}{:<80}{:<12}{:<12}".format("S.N", "ABONE NO", "ABONE ADI", "FAT.TUTARI", "ÖDE.TUTAR"))

		# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
		for file in files:
			birincisayfa = []			
			# pdfplumber modülü ile dosyaları açıyoruz
			with pdfplumber.open(file) as pdf:
				sayfa_1 = pdf.pages[0]
				birincisayfa.append(sayfa_1.extract_text())
			#print(birincisayfa)

			sira_no = sira_no + 1
			# Sayfaları Geziyoruz
			for sayfa in birincisayfa:
			# 	# Cami/Abone adını tespit etmek için kriter belirliyoruz.
				abone_eslesen = re.search("ADI\s(\D+)\\n", sayfa)
				abone_adi = ' '.join(abone_eslesen.groups()).strip().split("\n")[0]

				aboneadres_eslesen = re.search("MÜŞTERİ ADI(\D+)", sayfa)
				abone_adres = ' '.join(aboneadres_eslesen.groups()).split("\n")[1].split("ADRESİ")[-1].strip()

				abone_adi = abone_adi + " " + abone_adres

				# Toplam harcanan KWH
				kwh_eslesen = re.search("\\nTüketim.?([\d+]?[\d+]?[\d+]?\.?[\d+]?[\d+]?[\d+]?,?[\d+]?[\d+]?[\d+]?)", sayfa)
				if ' '.join(kwh_eslesen.groups()) == '':
					kwh_eslesen = re.search("\\nEner\D+(.*)\\nEner", sayfa)
					kwh_toplam = ' '.join(kwh_eslesen.groups()).split()[1].replace('.', '').replace(',', '.')
				else:
					kwh_toplam = float(' '.join(kwh_eslesen.groups()).replace('.', '').replace(',', '.').strip())

				# Fatura Tesisat/Tüketici Numarası
				tesisat_eslesen = re.search("SAT\D+(\d+)", sayfa)
				tesisat_no = ' '.join(tesisat_eslesen.groups()).strip()

				# Müsteri NO
				musteri_eslesen = re.search("NO.?(\d+)", sayfa)
				musteri_no = ' '.join(musteri_eslesen.groups())

				# Fatura Tesisat/Tüketici Numarası
				sozhesap_eslesen = re.search("ABI\s?(\d+)", sayfa)
				sozhesap_no = ' '.join(sozhesap_eslesen.groups()).strip()

				# Fatura No
				faturano_eslesen = re.search("\d+\s(Y.*el:)", sayfa)
				faturano = ' '.join(faturano_eslesen.groups()).strip()
				fatura_no = "YP" + ''.join([n for n in faturano if n.isdigit()])

				#SAP FATURA No için burayı aktif edin.
				#faturano_eslesen = re.search("SAP\D+NO\s?(\w+)", sayfa)
				#fatura_no = ' '.join(faturano_eslesen.groups()).strip()

				# Fatura Grubu
				faturagrubu_eslesen = re.search("GRUBU\D+\s(\D+)\s.G", sayfa)
				faturagrubu = ' '.join(faturagrubu_eslesen.groups()).strip()

				# Son ödeme Tarihi
				sonOdeme_eslesen = re.search("z\\n(\w+.\w+.\w+).?\\n|kWh\\n(\w+.\w+.\w+).?\\n|kWh\s(\w+.\w+.\w+).?\\n", sayfa)
				for sonOdeme in sonOdeme_eslesen.groups():
					if sonOdeme == None:
						continue
					else:
						sonOdeme_tarihi = sonOdeme

				# İlk Okuma Tarihi
				sonOkuma_eslesen = re.search("Son\sOkuma\s\w+\s(\w+.\w+.\w+)", sayfa)
				sonOkuma_tarihi = ' '.join(sonOkuma_eslesen.groups())

				# Son Okuma Tarihi
				ilkOkuma_eslesen = re.search(".lk\sOkuma\s\w+\s(\w+.\w+.\w+)", sayfa)
				ilkOkuma_tarihi = ' '.join(ilkOkuma_eslesen.groups())

				# Faturanın ait olduğu dönem
				faturaDonemi_ay = sonOkuma_tarihi.split(".")[1]
				faturaDonemi_yil = sonOkuma_tarihi.split(".")[-1]
				faturaDonemi = faturaDonemi_ay + "/" + faturaDonemi_yil

				# fatura tutarına binde 948 ile çarpılarak bulunmuştur.
				damgavergisi_eslesen = re.search("Damga\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
				damga_vergisi = round(float(damgavergisi_eslesen.group(1).strip()), 2)

				# fatura tutarına binde 948 ile çarpılarak bulunmuştur.
			 	###kdv_eslesen = re.search("KDV\s\S+\s([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
				kdv_eslesen = re.search("(KDV\s\S+\s?([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?))", sayfa)
				kdv = round(float(kdv_eslesen.groups(1)[0].split()[-1].strip().replace(".", "").replace(',', '.')), 2)

				# Gecikme Bedeli varsa
				gecikmebedeli_eslesen = re.search("Gecikme\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
				if gecikmebedeli_eslesen == None:
					gecikmebedeli = float(0)
				else:
					gecikmebedeli = float(gecikmebedeli_eslesen.group(1).strip())

				# Kesme/Açma Bedeli varsa
				kesacbedeli_eslesen = re.search("Kesme\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
				if kesacbedeli_eslesen == None:
					kesacbedeli = float(0)
				else:
					kesacbedeli = float(kesacbedeli_eslesen.group(1).strip())

				# trt fonu
				trtfonu_eslesen = re.search("TRT\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
				if trtfonu_eslesen == None:
					trtfonu = float(0)
				else:
					trtfonu = float(trtfonu_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

				# trt fonu
				trafo_eslesen = re.search("Trafo\D+Bo\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
				if trafo_eslesen == None:
					trafo = float(0)
				else:
					trafo = float(trafo_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

				# enerji fonu
				enerjifonu_eslesen = re.search("En\D+Fo\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
				if enerjifonu_eslesen == None:
					enerjifonu = float(0)
				else:
					enerjifonu = float(enerjifonu_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

				# Elektrik Tükeyim Vergisi
				elektriktuketimvergisi_eslesen = re.search("Elekt\D+T\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
				if elektriktuketimvergisi_eslesen == None:
					elektriktuketimvergisi = float(0)
				else:
					elektriktuketimvergisi = float(elektriktuketimvergisi_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

				TuketimTutari_eslesen = re.search("En\D+T\D+Be\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL|\\n([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL\\nAk|To\D+E\D+Be\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
				for ttutar in TuketimTutari_eslesen.groups():
					if ttutar == None:
						continue
					elif ttutar.startswith(",") or ttutar.startswith("."):
						continue
					elif len(ttutar) <= 2:
						continue
					else:
						tututar = float(ttutar.strip().replace(".", "").replace(',', '.'))
					TuketimTutari_1 = tututar
				TuketimTutari_2 = TuketimTutari_1 + gecikmebedeli + kesacbedeli + trafo
				faturaTutari = round((TuketimTutari_2 + kdv + trtfonu + enerjifonu + elektriktuketimvergisi), 2)

				#Bu alan, faturalarda belli bir standar olmadığı için hata veriyor
				#Eski Borç Tutarı
				#oncekiBorc_eslesen = re.search("\s\\n([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
				#if oncekiBorc_eslesen == None:
				#	oncekiBorcTutari = float(0)
				#else:
				#	oncekiBorcTutari = float(oncekiBorc_eslesen.group(1).replace(".", "").replace(',', '.').strip())

				# Ödenecek Tutar
				odenecekTutar = round((faturaTutari - damga_vergisi), 2)
			# Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
			print("{:<5}{:<18}{:<80}{:<12}{:<12}".format(sira_no, sozhesap_no, abone_adi, faturaTutari, odenecekTutar))

			# ornek_excel.xlsx dosyasının son satırını buluyoruz.
			son_satir = excel_sayfasi.max_row

			# ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
			excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
			excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(musteri_no)
			excel_sayfasi.cell(column=3, row=son_satir + 1).value = int(sozhesap_no)
			excel_sayfasi.cell(column=4, row=son_satir + 1).value = int(tesisat_no)
			excel_sayfasi.cell(column=5, row=son_satir + 1).value = fatura_no
			excel_sayfasi.cell(column=6, row=son_satir + 1).value = abone_adi
			excel_sayfasi.cell(column=7, row=son_satir + 1).value = faturagrubu
			excel_sayfasi.cell(column=8, row=son_satir + 1).value = faturaDonemi
			excel_sayfasi.cell(column=9, row=son_satir + 1).value = ilkOkuma_tarihi
			excel_sayfasi.cell(column=10, row=son_satir + 1).value = sonOkuma_tarihi
			excel_sayfasi.cell(column=11, row=son_satir + 1).value = sonOdeme_tarihi
			excel_sayfasi.cell(column=12, row=son_satir + 1).value = kwh_toplam
			excel_sayfasi.cell(column=13, row=son_satir + 1).value = faturaTutari
			excel_sayfasi.cell(column=14, row=son_satir + 1).value = damga_vergisi
			excel_sayfasi.cell(column=15, row=son_satir + 1).value = kdv
			excel_sayfasi.cell(column=16, row=son_satir + 1).value = gecikmebedeli
			excel_sayfasi.cell(column=17, row=son_satir + 1).value = kesacbedeli
			excel_sayfasi.cell(column=18, row=son_satir + 1).value = odenecekTutar

			# ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
			# pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
			excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')
	except:
		print("Dosya Adı: ", file)
		print(traceback.format_exc())

if __name__ == "__main__":
	fatura_oku()
