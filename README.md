# ELEKTRİK FATURASI OKUMA PROGRAMI

Ülkemizde faaliyet gösteren Elektirk Dağıtım Firmalarının MYS üzerinden gönderdiği elektrik faturalarını PYTHON dili kullanarak okuma ve bilgileri excele aktarma.....

Bu program özelde Diyanet İşleri Başkanlığı Taşra Teşkilatında görev yapan mutemet kardeşlerim için ödenek talep etmek için doldurulan form için hazırlanmıştır. Kaynak göstermek kaydı ile dilediğiniz gibi kullanmakta herhangi bir sakınca yoktur.

# DESTEKLENEN ŞİRKETLER:

* [ ] [1- Çoruh Elektrik Dağıtım A.Ş. (Trabzon, Giresun, Rize, Artvin ve Gümüşhane)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=CORUH-DEDAS-TREPAS-ARAS-MEPAS)
***
* [ ] [2- Dicle Elektrik Dağıtım A.Ş. (Şanlıurfa, Diyarbakır, Mardin, Batman, Şırnak ve Siirt)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=CORUH-DEDAS-TREPAS-ARAS-MEPAS)
***
* [ ] [3- Trakya Elektrik Dağıtım A.Ş. (Tekirdağ, Kırklareli ve Edirne)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=CORUH-DEDAS-TREPAS-ARAS-MEPAS)
***
* [ ] [4- Aras Elektrik Dağıtım A.Ş. (Erzurum, Ağrı, Kars, Erzincan, Iğdır, Ardahan ve Bayburt)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=CORUH-DEDAS-TREPAS-ARAS-MEPAS)
***
* [ ] [5- Meram Elektrik Dağıtım A.Ş. (Konya, Aksaray, Niğde, Nevşehir, Karaman ve Kırşehir)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=CORUH-DEDAS-TREPAS-ARAS-MEPAS)
***
* [ ] [6- Akedaş Elektrik Dağıtım A.Ş. (Kahramanmaraş ve Adıyaman)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=AKEDAS)
***
* [ ] [7- Osmangazi Elektrik Dağıtım A.Ş. (Eskişehir, Afyon, Kütahya, Uşak ve Bilecik)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=OSMANGAZI)
***
* [ ] [8- Başkent Elektrik Dağıtım A.Ş. (Ankara, Zonguldak, Kastamonu, Kırıkkale, Karabük, Çankırı ve Bartın)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=ENERJISA)
***
* [ ] [9- Sakarya Elektrik Dağıtım A.Ş. (Kocaeli, Sakarya, Düzce ve Bolu)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=SEPAS)
***
* [ ] [10- Yeşilırmak Elektrik Dağıtım A.Ş. (Samsun, Ordu, Çorum, Amasya ve Sinop)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=YEPAS)
***
* [ ] [11- Kayseri ve Civarı Elektrik Türk A.Ş. (Kayseri)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=KEPSAS)
***
* [ ] [12- Aydem Elektrik Dağıtım A.Ş. (Aydın, Denizli ve Muğla)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=AYDEM-GEDIZ)
***
* [ ] [13- Çamlıbel Elektrik Dağıtım A.Ş. (Sivas, Tokat ve Yozgat)](https://gitlab.com/yahyayildirim/fatura_oku/-/archive/main/fatura_oku-main.zip?path=CAMLIBEL-AKDENIZ)
***

# ÖN HAZIRLIK VE PYTHON KURULUM TALİMATI:
1 - Python versiyonununuz 3.x olması gerekiyor. Kontrol için aşağıdaki komutu uçbirimden çalışıtırın, sonucun Python 3.x.x gibi olması gerekiyor.
```
python3 --version
```

2 - uçbirimden python3-pip dosyasını kuruyoruz.
```
sudo apt install -f python3-pip
```

3 - Aşağıdaki kodları sırasıyla tek tek kuruyoruz.
***
**(Not: Kurulumda [SSL: CERTIFICATE_VERIFY_FAILED] hatası verirse, kurumun local ağında olduğunuz ve üst yönetim tarafından https://pypi.org/simple/ adresi beyaz listeye eklenmediği için hata veriyordur. Kurum BT yöneticisi ile irtibat kurmalısınız.)**
***
```
sudo python3 -m pip install --upgrade pip

sudo python3 -m pip install --upgrade setuptools

sudo python3 -m pip install --upgrade pdfplumber

sudo python3 -m pip install --upgrade openpyxl

sudo python3 -m pip install --upgrade typing_extensions

```

# ÇALIŞTIRMA TALİMATI:
1 - İndirip zip arşivinden çıkardığınız klasörün içine girin.

2 - PDF klasörüne faturaları **.pdf** uzantılı olacak şekilde kopyalayın.

3 - Son olarak fatura_oku.py uzantılı dosyaya çift tıklayın, çıkan uyarıya **"Uçbirimde Çalıştır"** butonuna tıklayın, uçbirim açılacak ve faturalar okunmaya başlayacaktır. Program kapandıktan sonra aynı klasörde *pdf_fatura_excel_aktarma_xxxxxx_xxxxxx.xlsx* adında bir excel dosyası oluşacaktır.

4 - Şayet dosya oluşmuyorsa veya tamamını okumadan kapanıyorsa program hata vermiş demektir. Bu sebeple hatanın kaynağını öğrenmek için uçbirimi programın olduğu klasörde açıp **./fatura_oku.py** ibaresini yazıp çalıştın ve çıkan hatayı bana gönderin. Vaktim olursa size dönüş yaparım inşallah...


*NOT_1 : Desteklenen Şirketler sadece kendi şirket isimleri ile ilgili olan dosyayı indirsinler.*

*NOT_2 : İsmi olmayan Müftülükler benimle iletişime geçerse, fırsat buldukça onlar içinde programı düzenlerim.*



**[Telegram İletişim](http://t.me/yahyayildirim)**
