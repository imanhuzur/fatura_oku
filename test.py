#!/bin/python3
# import xlsxwriter


# isim=["AHMET","HASAN","MEHMET"]
# iban = ["tr", "en", "rt"]
# tutar = [20, 30, 40]

# workbook = xlsxwriter.Workbook('benim_listem.xlsx')

# worksheet = workbook.add_worksheet()


# #birinci sütun(0) yani A sütununa isim_listesini yazdırma
# #worksheet.write(satir,0,veri) A=0 sütunu sabittir
 
# for satir,veri in enumerate(isim):
#     worksheet.write(satir,0,veri)
 
# #ikinci sütun(1)yani B sütununa soyisim_listesini yazdırma
# #worksheet.write(satir,1,veri) B=1 sütunu sabittir
 
# for satir,veri in enumerate(iban):
#     worksheet.write(satir,1,veri)

# for satir,veri in enumerate(tutar):
#     worksheet.write(satir,2,veri)

# #dosya işlemlerini bitiriyoruz.
# workbook.close()

import pandas as pd
import pdfplumber
import datetime
import re
import openpyxl
from pathlib import Path


# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

# PDF dosyasından çektiğimiz tarihlerin formatını değiştirme
def tarih_donustur(tarih):
        return re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', tarih)

# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
pdf_klasoru = Path('./PDF/').rglob('*.pdf')

# PDF Klasörü içindeki her bir dosyayı buluyoruz. 
files = [x for x in pdf_klasoru]

sira_no = 0
# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
for file in files:
    sayfalar = []

    # pdfplumber modülü ile dosyaları açıyoruz
    with pdfplumber.open(file) as pdf:
        for sayfa in pdf.pages:
            sayfalar.append(sayfa.extract_text().replace("\n", " "))
    #print(file)
    #print(sayfalar)

    
    # Text formatına çevirdiğimiz PDF dosyasındaki satırları tek tek işliyoruz
    for sayfa in sayfalar:
        sira_no = sira_no + 1

        abone_eslesen = re.search("SAYIN.*FATURA\s(\D+)Fa\D+Tipi", sayfa)
        abone_adi = ' '.join(abone_eslesen.groups()).strip()

    print(sira_no, abone_adi)
    # # ornek_excel.xlsx dosyasının son satırını buluyoruz.
    # son_satir = excel_sayfasi.max_row

    # # ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
    # excel_sayfasi.cell(column=1, row=son_satir + 1).value = fatura_no
    # excel_sayfasi.cell(column=2, row=son_satir + 1).value = devir



    # # ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
    # # pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
    # excel_dosyasi.save('pdf_fatura_excel_aktarma.xlsx')
