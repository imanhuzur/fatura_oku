#!/usr/bin/env python3

from datetime import datetime
from pathlib import Path
import pdfplumber
import openpyxl
import math
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
#################################################

bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

def tarih_donustur(tarih):
	return re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', tarih)

# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
pdf_klasoru = Path('./PDF/').rglob('*.pdf')
    # PDF Klasörü içindeki her bir dosyayı buluyoruz.
files = [x for x in pdf_klasoru]
    # PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
sira_no = 0

# Uçbirimde çıkacak sutun isimleri
print("{:<5}{:<12}{:<18}{:<40}{:<10}{:<12}{:<8}{:<8}{:<7}{:<10}{:<10}".format("S.N", "ABONE NO", "FATURA NO", "ABONE/CAMİİ ADI", "DÖNEM", "SON ÖDEME", "KWH", "KDV", "DAMGA", "ÖD.TUTAR", "ESKİ BORÇ"))

# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
for file in files:
	sayfalar = []
	# pdfplumber modülü ile dosyaları açıyoruz
	with pdfplumber.open(file) as pdf:
		ilk_sayfa = pdf.pages[0]
		sayfalar.append(ilk_sayfa.extract_text())

	#print(sayfalar)
	# devirGecikme = float(0)
	sira_no = sira_no + 1
	# # Sayfaları Geziyoruz
	for sayfa in sayfalar:

		# Cami/Abonenin ismi
		unvan_eslesen = re.search(r"\b(?:(.*)(.*)?\n(Çağrı))\b", sayfa)
		abone_adi = ' '.join(unvan_eslesen.groups()).split("54")[1].split("Çağrı")[0].strip()

		# Toplam harcanan KWH
		# kwh_liste = []
		# kwh_toplam = 0
		# kwh_eslesme = re.findall(r"(\d+.\d+.\d+).?KWH|(\d+.\d+).?KWH|(\d+).?KWH", sayfa)
		# for kwh in kwh_eslesme:
		# 	kwh_liste.append(float(' '.join(kwh).split()[0].replace('.', '').replace(',', '.').strip()))
		# 	kwh_toplam = round((sum(kwh_liste)), 3)

		kwh_liste = []
		kwh_toplam = 0
		kwh_eslesme = re.findall(r"ToplamTuketim.([0-9]+.?[0-9]+.?\d+)|(\d+.\d+.\d+).?KWH|(\d+.\d+).?KWH|(\d+).?KWH", sayfa)
		for kwh in kwh_eslesme:
			kwh_liste.append(float(' '.join(kwh).strip().split()[0].replace('.', '').replace(',', '.')))
			kwh_toplam = round((sum(kwh_liste)), 3)

		# Fatura Tesisat/Tüketici Numarası
		tesisat_re = "TESISATNO:\s(\d+)|Tüket.c. No:\s(\d+)"
		tesisat_eslesen = re.findall(tesisat_re, sayfa)
		for tesisat in tesisat_eslesen:
			tuketici_no = ' '.join(tesisat).strip()

		# Fatura No
		fatura_eslesen = re.search("Fatura.No[:]?[\s]?(\w+)", sayfa)
		fatura_no = ' '.join(fatura_eslesen.groups()).strip()

		# Sözleşme Hesap No
		ShesapNo_eslesen = re.search("Sözleşme(.[H]esap\\n\w+|.[H]esap.No[:]\w+|\\n\w+)", sayfa)
		ShesapNo = ' '.join(ShesapNo_eslesen.groups(1)).split()[-1].split(":")[-1]

		# Faturanın ait olduğu dönem
		faturaDonemi_eslesen = re.search("[O]\w{4}.[D].{7}(\d+\/\d+)", sayfa)
		faturaDonemi = ' '.join(faturaDonemi_eslesen.groups(1))

		# Faturadaki İlk Son Tarihi
		sonOkuma_re = "Son\sOkuma:\s\w+.\w+.\w+|Son\sOkuma\s\w+\s\w+.\w+.\w+"
		sonOkuma_eslesen = re.findall(sonOkuma_re, sayfa)
		for okuma in sonOkuma_eslesen:
			sonOkuma_tarihi = tarih_donustur(okuma.split()[-1].strip())

		# Faturadaki İlk Okuma Tarihi
		ilkOkuma_re = ".lk\sOkuma:\s\w+.\w+.\w+|.lk\sOkuma\s\w+\s\w+.\w+.\w+"
		ilkOkuma_eslesen = re.findall(ilkOkuma_re, sayfa)
		for okuma in ilkOkuma_eslesen:
			ilkOkuma_tarihi = tarih_donustur(okuma.split()[-1].strip())

		# Son ödeme tarihi satırı problemli, çift tarih var ve sayfa extract edilirken üst satırda kalıyor.
		# Bu sorunu ancak bu şekilde çözebildim.
		sonOdeme_eslesen = re.search("(\d{2}-.\d{2}\s\-\s\d{4})(\d+..\d+..)", sayfa)
		sonodeme_gun = sonOdeme_eslesen.groups(1)[0].split("-")[0].strip()
		sonodeme_ay = sonOdeme_eslesen.groups(1)[0].split("-")[1].strip()
		sonodeme_yil = sonOdeme_eslesen.groups(1)[0].split("-")[2].strip()
		sonOdeme_tarihi = sonodeme_gun + "." + sonodeme_ay + "." + sonodeme_yil

		# Kdv dahil fatura tutar
		faturaTutari_eslesen = re.search("[F]\w{5}.[T]\w{5}.([0-9]+.?[0-9]+.?\d+).TL", sayfa)
		faturaTutari = float(' '.join(faturaTutari_eslesen.groups(1)).strip().replace('.', '').replace(',', '.').strip())

		# fatura tutarına binde 948 ile çarpılarak bulunmuştur.
		damga_vergisi = round(((faturaTutari/1.18)*0.00948), 2)

		# Fatura tutarını 1.18 e bölüp yüzde 18 ile çarparak bulunmuştur.
		kdv = round(((faturaTutari/1.18)*0.18), 2)

		oncekiBorc_eslesen = re.search("[F]\w{5}.[B]\w{4}...([0-9]+.?[0-9]+.?\d+).TL", sayfa)
		oncekiBorcTutari = float(' '.join(oncekiBorc_eslesen.groups(1)).strip().replace('.', '').replace(',', '.').strip())

		# Eğer varsa önceki döneme ait borç
		odenecekTutar_eslesen = re.search("\w{7}.[T]\w{4}.([0-9]+.?[0-9]+.?\d+).TL", sayfa)
		odenecekTutar = float(' '.join(odenecekTutar_eslesen.groups(1)).strip().replace('.', '').replace(',', '.').strip())

		
	# 	devirGecikme_re = "Devir Gecikme\s.*\sTL"
	# 	devirGecikme_eslesen = re.findall(devirGecikme_re, sayfa)
	# 	for devir in devirGecikme_eslesen:
	# 		devirGecikme = float(devir.split()[-2].strip().replace('.', '').replace(',', '.'))

	# # # Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
	print("{:<5}{:<12}{:<18}{:<40}{:<10}{:<12}{:<8}{:<8}{:<7}{:<10}{:<10}".format(sira_no, tuketici_no, fatura_no, abone_adi, faturaDonemi, sonOdeme_tarihi, kwh_toplam, kdv, damga_vergisi, odenecekTutar, oncekiBorcTutari))

	# # # ornek_excel.xlsx dosyasının son satırını buluyoruz.
	son_satir = excel_sayfasi.max_row

	# # # ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
	excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
	excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(tuketici_no)
	excel_sayfasi.cell(column=3, row=son_satir + 1).value = int(ShesapNo)
	excel_sayfasi.cell(column=4, row=son_satir + 1).value = fatura_no
	excel_sayfasi.cell(column=5, row=son_satir + 1).value = abone_adi
	excel_sayfasi.cell(column=6, row=son_satir + 1).value = faturaDonemi
	excel_sayfasi.cell(column=7, row=son_satir + 1).value = ilkOkuma_tarihi
	excel_sayfasi.cell(column=8, row=son_satir + 1).value = sonOkuma_tarihi
	excel_sayfasi.cell(column=9, row=son_satir + 1).value = sonOdeme_tarihi
	excel_sayfasi.cell(column=10, row=son_satir + 1).value = kwh_toplam
	excel_sayfasi.cell(column=11, row=son_satir + 1).value = faturaTutari
	excel_sayfasi.cell(column=12, row=son_satir + 1).value = damga_vergisi
	excel_sayfasi.cell(column=13, row=son_satir + 1).value = kdv
	excel_sayfasi.cell(column=14, row=son_satir + 1).value = odenecekTutar
	excel_sayfasi.cell(column=15, row=son_satir + 1).value = oncekiBorcTutari

	# # # ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
	# # # pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
	excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')

