#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from pathlib import Path
import pdfplumber
import traceback
import openpyxl
import math
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
#################################################

bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

def fatura_oku():
	try:

		# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
		# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
		pdf_klasoru = Path('./PDF/').rglob('*.pdf')

		# PDF Klasörü içindeki her bir dosyayı buluyoruz. 
		files = [x for x in pdf_klasoru]

		# PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
		sira_no = 0

		# Uçbirimde çıkacak sutun isimleri
		print("{:<5}{:<18}{:<45}{:<10}".format("S.N", "SÖZ./ABONE NO", "CAMİ/ABONE ADI", "ÖD.TUTAR"))

		# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
		for file in files:
			birincisayfa = []
			# pdfplumber modülü ile dosyaları açıyoruz
			with pdfplumber.open(file) as pdf:
				sayfa_1 = pdf.pages[0]
				birincisayfa.append(sayfa_1.extract_text())
			#print(birincisayfa)

			# Her bir pdf dosyası okununca sira numarası bir artacaktır.
			sira_no = sira_no + 1

			for sayfa in birincisayfa:
				# Abone Unvanı
				abone_eslesen = re.search("SOYADI(\D+)\\n", sayfa)
				abone_adi = ' '.join(abone_eslesen.groups()).strip()

				#Abone Adresi
				#abone_eslesen = re.search("SAT.AD\w+(.*)ETTN", sayfa)
				#abone_adi = ' '.join(abone_eslesen.groups()).strip()
				
				# Toplam harcanan KWH
				kwh_eslesen = re.search("Aktif\D+\(kWh\)\s?([1-9]\d{0,2}(.?\d{1,3})*|([1-9]\d*))(.?\d{2})?", sayfa)
				kwh_toplam = kwh_eslesen.group(1).strip().replace(".", "").replace(",", ".")

				# Sözleşme Hesap No
				sozhesap_eslesen = re.search("NO\s?(\d+)", sayfa)
				sozhesap_no = ' '.join(sozhesap_eslesen.groups()).strip()

				# Tesisat No - Tekil Kod
				tesisat_eslesen = re.search("(\d{8,10})(\D+)?\\nKOD|KOD\s?(\d+)", sayfa)
				tesisat_no_1 = tesisat_eslesen.group(1)
				tesisat_no_2 = tesisat_eslesen.group(3)
				if not tesisat_no_1 == None:
					tesisat_no = int(tesisat_eslesen.group(1))
				else:
					tesisat_no = int(tesisat_eslesen.group(3))

				# Abonelik Grubu
				abonegrubu_eslesen = re.search("GRUBU\s?[0-9]*(\D+)\sTel", sayfa)
				abonegrubu = ' '.join(abonegrubu_eslesen.groups()).strip().split()[-1]

				# Fatura No
				fatura_eslesen = re.search("Fatura.No[:]?[\s]?(\w+)", sayfa)
				fatura_no = ' '.join(fatura_eslesen.groups()).strip()

				# Faturanın ait olduğu dönem
				faturaDonemi_eslesen = re.search("Fatura\D+\s?(\d{4}\/\d+)", sayfa)
				faturaDonemi = ' '.join(faturaDonemi_eslesen.groups()).strip()

				# Faturanın İlk Okuma Tarihi
				ilkOkuma_eslesen = re.search(".lk\sOk\D+(\d+.\d+.\d+)", sayfa)
				ilkOkuma_tarihi = ' '.join(ilkOkuma_eslesen.groups()).strip()

				# Faturanın Son Okuma Tarihi
				sonOkuma_eslesen = re.search("Son\sOk\D+(\d+.\d+.\d+)", sayfa)
				sonOkuma_tarihi = ' '.join(sonOkuma_eslesen.groups()).strip()
				
				# Son Ödeme Tarihi
				sonOdeme_eslesen = re.search("SON\D+(\d+.\d+.\d+)", sayfa)
				sonOdeme_tarihi = ' '.join(sonOdeme_eslesen.groups()).strip()
				
				# Eski Borç Tutarı
				oncekiBorc_eslesen = re.search("ESK\D+([0-9]\d{0,2}(.?\d{1,3})*|([1-9]\d*))(.?\d{2})?\s?\\n", sayfa)
				if oncekiBorc_eslesen == None:
					oncekiBorcTutari = float(0)
				else:
					oncekiBorcTutari = float(oncekiBorc_eslesen.group(1).strip().replace(".", "").replace(",", "."))
				
				# Fatura Tutarı
				faturaTutari_eslesen = re.search("FA\D+TU\D+([0-9]\d{0,2}(.?\d{1,3})*|([1-9]\d*))(.?\d{2})?", sayfa)
				faturaTutari = float(faturaTutari_eslesen.group(1).strip().replace(".", "").replace(",", "."))
				
				# Damga Vergisi
				damga_vergisi = round(((faturaTutari/1.18)*0.00948),2)
				
				# KDV
				kdv = round(((faturaTutari/1.18)*0.18),2)
				
				# Ödenecek Tutar
				odenecekTutar_eslesen = re.search("\\n([0-9]\d{0,2}(.?\d{1,3})*|([1-9]\d*))(.?\d{2})?\sTL", sayfa)
				odenecekTutar = float(odenecekTutar_eslesen.group(1).strip().replace(".", "").replace(",", "."))

				
			# # Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
			print("{:<5}{:<18}{:<45}{:<10}".format(sira_no, sozhesap_no, abone_adi, odenecekTutar))

			# ornek_excel.xlsx dosyasının son satırını buluyoruz.
			son_satir = excel_sayfasi.max_row

			# ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
			excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
			excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(sozhesap_no)
			excel_sayfasi.cell(column=3, row=son_satir + 1).value = int(tesisat_no)
			excel_sayfasi.cell(column=4, row=son_satir + 1).value = fatura_no
			excel_sayfasi.cell(column=5, row=son_satir + 1).value = abone_adi
			excel_sayfasi.cell(column=6, row=son_satir + 1).value = abonegrubu
			excel_sayfasi.cell(column=7, row=son_satir + 1).value = datetime.strptime(faturaDonemi, "%Y/%m").strftime('%Y/%m')
			excel_sayfasi.cell(column=8, row=son_satir + 1).value = datetime.strptime(ilkOkuma_tarihi, "%d.%m.%Y").strftime('%Y/%m/%d')
			excel_sayfasi.cell(column=9, row=son_satir + 1).value = datetime.strptime(sonOkuma_tarihi, "%d.%m.%Y").strftime('%Y/%m/%d')
			excel_sayfasi.cell(column=10, row=son_satir + 1).value = datetime.strptime(sonOdeme_tarihi, "%d.%m.%Y").strftime('%Y/%m/%d')
			excel_sayfasi.cell(column=11, row=son_satir + 1).value = kwh_toplam
			excel_sayfasi.cell(column=12, row=son_satir + 1).value = faturaTutari
			excel_sayfasi.cell(column=13, row=son_satir + 1).value = damga_vergisi
			excel_sayfasi.cell(column=14, row=son_satir + 1).value = kdv
			excel_sayfasi.cell(column=15, row=son_satir + 1).value = odenecekTutar
			excel_sayfasi.cell(column=16, row=son_satir + 1).value = oncekiBorcTutari

			# ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
			# pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
			excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')

	except:
		print("Hata Veren PDF Dosyası :", file)
		print(traceback.format_exc())

if __name__ == "__main__":
	fatura_oku()
