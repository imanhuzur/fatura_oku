#!/usr/bin/env python3

from datetime import datetime
from pathlib import Path
import pdfplumber
import traceback
import openpyxl
import math
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
#################################################

#Tarih
bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

# PDF dosyasından çektiğimiz tarihlerin formatını değiştirme
def tarih_donustur(tarih):
        return re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', tarih)

def fatura_oku():
    try:
        # PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
        # Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
        pdf_klasoru = Path('./PDF/').rglob('*.pdf')

        # PDF Klasörü içindeki her bir dosyayı buluyoruz. 
        files = [x for x in pdf_klasoru]

        sira_no = 0

        # Uçbirimde çıkacak sutun isimleri
        print("{:<5}{:<18}{:<40}{:<12}".format("S.N", "ABONE NO", "ABONE ADI", "ÖDE.TUTAR"))

        # for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
        for file in files:
            sayfalar = []

            # pdfplumber modülü ile dosyaları açıyoruz
            with pdfplumber.open(file) as pdf:
                for sayfa in pdf.pages:
                    sayfalar.append(sayfa.extract_text().replace("\n", " "))
            #print(file)
            #print(sayfalar)

            
            # Text formatına çevirdiğimiz PDF dosyasındaki satırları tek tek işliyoruz
            for sayfa in sayfalar:
                sira_no = sira_no + 1

                abone_eslesen = re.search("SAYIN.*FATURA\s(\D+)Fa\D+Tipi", sayfa)
                unvan_temizle = ' '.join(abone_eslesen.groups()).split(" AYD")[0]
                if "CAMİ" in unvan_temizle or "CAMI" in unvan_temizle:
                    abone_adi = f"{unvan_temizle.split('CAMİ')[0].split('CAMI')[0].strip()} CAMİİ"
                else:
                    abone_adi = unvan_temizle

                kwh_liste = []
                kwh_toplam = 0
                kwh_eslesme = re.findall(r"ToplamTuketim.([0-9]+.?[0-9]+.?\d+)|(\d+.\d+.\d+).?KWH|(\d+.\d+).?KWH|(\d+).?KWH", sayfa)
                for kwh in kwh_eslesme:
                    kwh_liste.append(float(' '.join(kwh).strip().split()[0].replace('.', '').replace(',', '.')))
                    kwh_toplam = round((sum(kwh_liste)), 3)

                tesisat_eslesen = re.findall("TESISATNO.?\s(\d+)|Tüket.c. No.?\s(\d+)", sayfa)
                for tesisat in tesisat_eslesen:
                    tuketici_no = ' '.join(tesisat).strip()

                fatura_eslesen = re.findall("Fatura\sNo[:]?\s\w+", sayfa)
                for fatura in fatura_eslesen:
                    fatura_no = fatura.split()[-1].strip()

                grubu_eslesen = re.search("Grubu:(\D+)\s?AG", sayfa)
                if grubu_eslesen == None:
                    grubu = ""
                else:
                    grubu = ' '.join(grubu_eslesen.groups(1)).strip().split()[-1]

                sonOkuma_eslesen = re.search("Son\sOkuma:\s(\w+.\w+.\w+)|Son\sOkuma\s\w+\s\w+.\w+.\w+|ILKOKUMA.?\w+.\w+.\w+", sayfa)
                if sonOkuma_eslesen == None:
                    sonOkuma_tarihi = ""
                else:
                    sonOkuma_tarihi = tarih_donustur(' '.join(sonOkuma_eslesen.groups()))

                ilkOkuma_eslesen = re.search(".lk\sOkuma:\s(\w+.\w+.\w+)|.lk\sOkuma\s\w+\s\w+.\w+.\w+|SONOKUMA.?\w+.\w+.\w+", sayfa)
                if ilkOkuma_eslesen == None:
                    ilkOkuma_tarihi = ""
                else:
                    ilkOkuma_tarihi = tarih_donustur(' '.join(ilkOkuma_eslesen.groups()))

                sonOdeme_eslesen = re.search("Son\D+Ta\D+(\w+.\w+.\w+)", sayfa)
                sonOdeme_tarihi = tarih_donustur(' '.join(sonOdeme_eslesen.groups()))

                faturadonemi_eslesen = re.search("\(Dönem:(.*),\sT", sayfa)
                if sonOkuma_tarihi == "":
                    faturaDonemi = ' '.join(faturadonemi_eslesen.groups())
                else:
                    faturaDonemi_yil = sonOkuma_tarihi.split("-")[-1]
                    faturaDonemi_ay = sonOkuma_tarihi.split("-")[-2]
                    faturaDonemi = faturaDonemi_yil + "/" + faturaDonemi_ay

                mal_hizmet_toplam_tutar = re.search("[M]\D+[H]\D+[T]o\D+[T]u\D+(.*)\sTL", sayfa)
                hizmet_tutari = float(' '.join(mal_hizmet_toplam_tutar.groups()).split()[0].strip().replace('.', '').replace(',', '.').strip())

                vergi_dahil_toplam_tutar = re.search("[V]e\D+[D]a\D+[T]o\D+[T]u\D+(.*)\sTL", sayfa)
                toplam_tutar = float(' '.join(vergi_dahil_toplam_tutar.groups()).split()[0].strip().replace('.', '').replace(',', '.').strip())
                damga_vergisi = round(((toplam_tutar/1.18)*0.00948), 2)
                kdv = round(((toplam_tutar/1.18)*0.18), 2)

                odenecek_eslesen = re.search(".denecek\s.*\s([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
                odenecek_tutar = float(odenecek_eslesen.group(1).strip().replace('.', '').replace(',', '.').strip())

            # Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
            print("{:<5}{:<18}{:<40}{:<12}".format(sira_no, tuketici_no, abone_adi, odenecek_tutar))

            # ornek_excel.xlsx dosyasının son satırını buluyoruz.
            son_satir = excel_sayfasi.max_row

            # ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
            excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
            excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(tuketici_no)
            excel_sayfasi.cell(column=3, row=son_satir + 1).value = fatura_no
            excel_sayfasi.cell(column=4, row=son_satir + 1).value = abone_adi
            excel_sayfasi.cell(column=5, row=son_satir + 1).value = grubu
            excel_sayfasi.cell(column=6, row=son_satir + 1).value = faturaDonemi
            excel_sayfasi.cell(column=7, row=son_satir + 1).value = ilkOkuma_tarihi
            excel_sayfasi.cell(column=8, row=son_satir + 1).value = sonOkuma_tarihi
            excel_sayfasi.cell(column=9, row=son_satir + 1).value = sonOdeme_tarihi
            excel_sayfasi.cell(column=10, row=son_satir + 1).value = kwh_toplam
            excel_sayfasi.cell(column=11, row=son_satir + 1).value = hizmet_tutari
            excel_sayfasi.cell(column=12, row=son_satir + 1).value = toplam_tutar
            excel_sayfasi.cell(column=13, row=son_satir + 1).value = damga_vergisi
            excel_sayfasi.cell(column=14, row=son_satir + 1).value = kdv
            excel_sayfasi.cell(column=15, row=son_satir + 1).value = odenecek_tutar


            # ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
            # pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
            excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')

    except:
        print("Dosya Adı: ", file)
        print(traceback.format_exc())

if __name__ == "__main__":
    fatura_oku()