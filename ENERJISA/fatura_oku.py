#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from pathlib import Path
import pdfplumber
import traceback
import openpyxl
import math
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
#################################################

bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

# def tarih_donustur(tarih):
# 	return re.sub(r'(\d{4}).?(\d{1,2}).?(\d{1,2})', '\\3.\\2.\\1', tarih)

def fatura_oku():
	try:

		# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
		# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
		pdf_klasoru = Path('./PDF/').rglob('*.pdf')

		# PDF Klasörü içindeki her bir dosyayı buluyoruz.
		files = [x for x in pdf_klasoru]

		# PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
		sira_no = 0

		# Uçbirimde çıkacak sutun isimleri
		print("{:<5}{:<18}{:<40}{:<10}".format("S.N", "SÖZ./ABONE NO", "CAMİ/ABONE ADI", "ÖD.TUTAR"))

		# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
		for file in files:
			birincisayfa, ikincisayfa, ucuncusayfa = [], [], []
			# pdfplumber modülü ile dosyaları açıyoruz
			with pdfplumber.open(file) as pdf:
				sayfa_1 = pdf.pages[0]
				#sayfa_2 = pdf.pages[1]
				#sayfa_3 = pdf.pages[2]
				birincisayfa.append(sayfa_1.extract_text())
				#ikincisayfa.append(sayfa_2.extract_text())
				#ucuncusayfa.append(sayfa_3.extract_text())
			#print(birincisayfa)

			# Önceki dönemden kalan borç yok ise 0.00 değeri dönecektir.	
			oncekiBorcTutari = float(0)

			# Her bir pdf dosyası okununca sira numarası bir artacaktır.
			sira_no = sira_no + 1
			
			# Sayfaları Geziyoruz
			for sayfa in birincisayfa:

				# # Cami/Abone adını tespit etmek için kriter belirliyoruz.
				# abone_eslesen = re.search("ADI SOYADI :(.*)\\nVERGİ\s", sayfa)
				# abone_adi = ' '.join(abone_eslesen.groups()).split("e-Fatura")[0].strip()

				adres_eslesen = re.search("[Aa][Dd][Rr][Ee][Ss].?\s:?(\D+)", sayfa)
				adres = ' '.join(adres_eslesen.groups()).split("Tic.")[0].split("NO:")[0].split("Fatura")[0].split("\n")[0].strip()

				# Toplam harcanan KWH
				kwh_eslesen = re.search(r"\(ETB\)\s([\d+]?[\d+]?[\d+]?\.?[\d+]?[\d+]?[\d+]?,?[\d+]?[\d+]?[\d+]?)", sayfa)
				kwh_toplam = float(' '.join(kwh_eslesen.groups()).replace('.', '').replace(',', '.').strip())

				# Fatura Tesisat/Tüketici Numarası
				sozhesap_eslesen = re.search("[EBPS].*.?[H]\w{4}.?[N].\s:(\d+)", sayfa)
				sozhesap_no = ' '.join(sozhesap_eslesen.groups()).strip()

				# Fatura Tesisat/Tüketici Numarası
				tesisat_eslesen = re.search("[T]E\w.*\s\/\s[T].*:(\d{10})", sayfa)
				tesisat_no = ' '.join(tesisat_eslesen.groups()).strip()

				# Müşteri No
				musteri_eslesen = re.search("[T]\D{3}\sNO\s:(\d{9})", sayfa)
				musteri_no = ' '.join(musteri_eslesen.groups()).strip()

				# Fatura No
				fatura_eslesen = re.search("[F]\w{5}\s[S].\w+\sNo[:]?[\s]?(\w+)", sayfa)
				fatura_no = ' '.join(fatura_eslesen.groups()).strip()

				# Faturanın ait olduğu dönem
				faturaDonemi_eslesen = re.search(":(\D{4,10}\s\d{4})", sayfa)
				faturaDonemi = ' '.join(faturaDonemi_eslesen.groups()).strip()

				# KDV Dahil Fatura Tutarı 
				faturaTutari_eslesen = re.search("[T|F]\w{5}.[T]\w{4,5}.([0-9]+.?[0-9]+.?\d+)", sayfa)
				faturaTutari = float(' '.join(faturaTutari_eslesen.groups()).replace('.', '').replace(',', '.').strip())

				# fatura tutarına binde 948 ile çarpılarak bulunmuştur.
				damga_vergisi = round(((faturaTutari/1.18)*0.00948), 2)

				# Fatura tutarını 1.18 e bölüp yüzde 18 ile çarparak bulunmuştur.
				kdv = round(((faturaTutari/1.18)*0.18), 2)

				# Faturanın İlk ve Son Okuma Tarihleri
				ilksonOkuma_eslesen = re.search("(\d+\.\d+\.\d+).\/.(\d+\.\d+\.\d+)", sayfa)
				ilkOkuma_tarihi = ' '.join(ilksonOkuma_eslesen.groups()).split()[0]
				sonOkuma_tarihi = ' '.join(ilksonOkuma_eslesen.groups()).split()[1]

				# Eğer varsa önceki döneme ait borç
				oncekiBorc_eslesen = re.findall("\(TL\)\s([0-9]+.?[0-9]+.?\d+)", sayfa)
				for borc in oncekiBorc_eslesen:
					oncekiBorcTutari = float(' '.join(oncekiBorc_eslesen).replace('.', '').replace(',', '.').strip())

				# Ödenecek Tutar
				odenecekTutar_eslesen = re.search("\s[T]\w{4}\s*([0-9]+.?[0-9]+.?\d+)", sayfa)
				odenecekTutar = float(' '.join(odenecekTutar_eslesen.groups(1)).split()[0].replace('.', '').replace(',', '.').strip())

				# Son Ödeme Tarihi
				sonOdemetarihi_eslesen = re.search("[S]ON\D+\D{6}\s(\d+\.\d+\.\d+)", sayfa)
				sonOdeme_tarihi = ' '.join(sonOdemetarihi_eslesen.groups(1)).split()[-1]


			# Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
			print("{:<5}{:<18}{:<40}{:<10}".format(sira_no, sozhesap_no, adres, odenecekTutar))

			# ornek_excel.xlsx dosyasının son satırını buluyoruz.
			son_satir = excel_sayfasi.max_row

			# ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
			excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
			excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(sozhesap_no)
			excel_sayfasi.cell(column=3, row=son_satir + 1).value = int(tesisat_no)
			excel_sayfasi.cell(column=4, row=son_satir + 1).value = fatura_no
			excel_sayfasi.cell(column=5, row=son_satir + 1).value = adres
			excel_sayfasi.cell(column=6, row=son_satir + 1).value = faturaDonemi
			excel_sayfasi.cell(column=7, row=son_satir + 1).value = ilkOkuma_tarihi
			excel_sayfasi.cell(column=8, row=son_satir + 1).value = sonOkuma_tarihi
			excel_sayfasi.cell(column=9, row=son_satir + 1).value = sonOdeme_tarihi
			excel_sayfasi.cell(column=10, row=son_satir + 1).value = kwh_toplam
			excel_sayfasi.cell(column=11, row=son_satir + 1).value = faturaTutari
			excel_sayfasi.cell(column=12, row=son_satir + 1).value = damga_vergisi
			excel_sayfasi.cell(column=13, row=son_satir + 1).value = kdv
			excel_sayfasi.cell(column=14, row=son_satir + 1).value = odenecekTutar
			excel_sayfasi.cell(column=15, row=son_satir + 1).value = oncekiBorcTutari

			# ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
			# pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
			excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')
	except:
		print("Dosya Adı: ", file)
		print(traceback.format_exc())

if __name__ == "__main__":
	fatura_oku()
