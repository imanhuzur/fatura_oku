#!/usr/bin/env python3

from datetime import datetime
from pathlib import Path
import pdfplumber
import traceback
import openpyxl
import math
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
#################################################

bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

def tarih_donustur(tarih):
	return re.sub(r'(\d{4}).?(\d{1,2}).?(\d{1,2})', '\\3.\\2.\\1', tarih)
def fatura_oku():
	try:
		# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
		# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
		pdf_klasoru = Path('./PDF/').rglob('*.pdf')
		    # PDF Klasörü içindeki her bir dosyayı buluyoruz.
		files = [x for x in pdf_klasoru]
		    # PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
		sira_no = 0

		# Uçbirimde çıkacak sutun isimleri
		print("{:<3}{:<14}{:<55}{:<10}".format("S.N", "SÖZ./ABONE NO", "CAMİ/ABONE ADI", "TUTAR"))

		# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
		for file in files:
			birincisayfa, ikincisayfa = [], []
			# pdfplumber modülü ile dosyaları açıyoruz
			with pdfplumber.open(file) as pdf:
				sayfa_1 = pdf.pages[0]
				sayfa_2 = pdf.pages[1]
				birincisayfa.append(sayfa_1.extract_text())
				ikincisayfa.append(sayfa_2.extract_text())
			#print(birincisayfa)

			oncekiBorcTutari = float(0)
			sira_no = sira_no + 1
			# Sayfaları Geziyoruz
			for sayfa in birincisayfa:
				# Cami/Abone adını tespit etmek için kriter belirliyoruz.
				abone_eslesen = re.search("SAYIN\\n(\D+)", sayfa)
				abone_adi = ' '.join(' '.join(abone_eslesen.groups()).strip().split("Özel")[0].split("\n")[:2])

				# Toplam harcanan KWH
				kwh_liste = []
				kwh_toplam = 0
				kwh_eslesme = re.findall(r"[1-9]+.?[0-9]*?.?[0-9]+.?KWH", sayfa)
				for kwh in kwh_eslesme:
					kwh_liste.append(float(kwh.split()[0].replace('.', '').replace(',', '.').strip()))
					kwh_toplam = round((sum(kwh_liste)), 3)

				# Fatura Tesisat/Tüketici Numarası
				tesisat_eslesen = re.search("Tesisat\D+(\d+)", sayfa)
				tesisat_no = ' '.join(tesisat_eslesen.groups()).strip()

				# Fatura Tesisat/Tüketici Numarası
				sozhesap_eslesen = re.search("Hesap\s\w{8}\D+(\d+)", sayfa)
				sozhesap_no = ' '.join(sozhesap_eslesen.groups()).strip()

				# Fatura No
				fatura_eslesen = re.search("Fatura.No[:]?[\s]?(\w+)", sayfa)
				fatura_no = ' '.join(fatura_eslesen.groups()).strip()

				# Faturanın ait olduğu dönem
				faturaDonemi_eslesen = re.search("[O]\w{4}.[D].*\s(\d+\/\d+)", sayfa)
				faturaDonemi = ' '.join(faturaDonemi_eslesen.groups(1))

				# Son ödeme tarihi satırı iki satırlı olduğu için sayfa extract edilirken üst satırda kalıyor.
				sonOdeme_eslesen = re.search("Son\s.deme\\n(\w+.\w+.\w+)", sayfa)
				sonOdeme_tarihi = ' '.join(sonOdeme_eslesen.groups(1))

				faturaTutari_eslesen = re.search("[T|F]\w{5}.[T]\w{4}.([0-9]+.?[0-9]+.?\d+)", sayfa)
				faturaTutari = float(' '.join(faturaTutari_eslesen.groups()).replace(",", ".").replace(',', '.').strip())

				# fatura tutarına binde 948 ile çarpılarak bulunmuştur.
				damga_vergisi = round(((faturaTutari/1.18)*0.00948), 2)

				# Fatura tutarını 1.18 e bölüp yüzde 18 ile çarparak bulunmuştur.
				kdv = round(((faturaTutari/1.18)*0.18), 2)

				# Eğer varsa önceki döneme ait borç
				oncekiBorc_eslesen = re.search("[V]\w{5}.[G]\w{5}.[B]\w+.([0-9]+.?[0-9]+.?\d+)", sayfa)
				oncekiBorcTutari = float(' '.join(oncekiBorc_eslesen.groups(1)).strip().replace('.', '').replace(',', '.').strip())

				# Ödenecek Tutar
				odenecekTutar_re = ".denecek\s[T]\w{4}\s([0-9]+.?[0-9]+.?\d+)"
				odenecekTutar_eslesen = re.findall(odenecekTutar_re, sayfa)
				odenecekTutar = float(' '.join(odenecekTutar_eslesen).replace('.', '').replace(',', '.').strip())

				# Müşteri Grubu
				grubu_eslesen = re.search("Grubu:(\D+)\s?AG", sayfa)
				grubu = ' '.join(grubu_eslesen.groups(1)).strip().split()[-1]

			# Bu alan faturanın ikinci sayfasındaki ilk okuma ve son okuma tarihleri içindir.
			# Eğer fatura tek sayfalı ise hata verecektir. 
			for sayfa in ikincisayfa:
				# Faturanın ikinci sayfasındaki İlk Okuma Tarihi
				sonOkuma_re = "Son\sOkuma:\s\w+.\w+.\w+|Son\sOkuma\s\w+\s\w+.\w+.\w+"
				sonOkuma_eslesen = re.findall(sonOkuma_re, sayfa)
				for okuma in sonOkuma_eslesen:
					sonOkuma_tarihi = tarih_donustur(okuma.split()[-1].strip())

				# Faturanın ikinci sayfasındaki Son Okuma Tarihi
				ilkOkuma_re = ".lk\sOkuma:\s\w+.\w+.\w+|.lk\sOkuma\s\w+\s\w+.\w+.\w+"
				ilkOkuma_eslesen = re.findall(ilkOkuma_re, sayfa)
				for okuma in ilkOkuma_eslesen:
					ilkOkuma_tarihi = tarih_donustur(okuma.split()[-1].strip())


			# Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
			print("{:<3}{:<14}{:<55}{:<10}".format(sira_no, sozhesap_no, abone_adi, odenecekTutar))

			# ornek_excel.xlsx dosyasının son satırını buluyoruz.
			son_satir = excel_sayfasi.max_row

			# ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
			excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
			excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(sozhesap_no)
			excel_sayfasi.cell(column=3, row=son_satir + 1).value = int(tesisat_no)
			excel_sayfasi.cell(column=4, row=son_satir + 1).value = fatura_no
			excel_sayfasi.cell(column=5, row=son_satir + 1).value = abone_adi
			excel_sayfasi.cell(column=6, row=son_satir + 1).value = grubu
			excel_sayfasi.cell(column=7, row=son_satir + 1).value = faturaDonemi
			excel_sayfasi.cell(column=8, row=son_satir + 1).value = ilkOkuma_tarihi
			excel_sayfasi.cell(column=9, row=son_satir + 1).value = sonOkuma_tarihi
			excel_sayfasi.cell(column=10, row=son_satir + 1).value = sonOdeme_tarihi
			excel_sayfasi.cell(column=11, row=son_satir + 1).value = kwh_toplam
			excel_sayfasi.cell(column=12, row=son_satir + 1).value = faturaTutari
			excel_sayfasi.cell(column=13, row=son_satir + 1).value = damga_vergisi
			excel_sayfasi.cell(column=14, row=son_satir + 1).value = kdv
			excel_sayfasi.cell(column=15, row=son_satir + 1).value = odenecekTutar
			excel_sayfasi.cell(column=16, row=son_satir + 1).value = oncekiBorcTutari

			# ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
			# pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
			excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')
	except:
		print("Dosya Adı: ", file)
		print(traceback.format_exc())

if __name__ == "__main__":
	fatura_oku()
